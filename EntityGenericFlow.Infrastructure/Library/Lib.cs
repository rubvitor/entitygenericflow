﻿using System.Collections.Generic;

namespace EntityGenericFlow.Infrastructure.Library
{
    public class Lib
    {
        public class Collection<T>
        {
            public int Total { get; set; }
            public List<T> List { get; set; }
            public string Message { get; set; }
            public bool Success { get; set; }

            public Collection()
            {
                List = new List<T>();
                Success = true;
            }
        }
        public class Entity<T>
        {
            public T Object { get; set; }
            public string Message { get; set; }
            public bool Success { get; set; }

            public Entity()
            {
                Success = true;
            }
        }
    }
}
