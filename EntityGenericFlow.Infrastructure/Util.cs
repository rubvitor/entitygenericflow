﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace EntityGenericFlow.Infrastructure
{
    public class Util
    {
        public static DateTime DateTimeFromTimestamp(long date)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return (epoch.AddSeconds((date / 1000)));
        }

        public static DateTime DateTimeFromTimestamp(long date, string timeZone)
        {
            TimeZoneInfo timezone;

            try
            {
                timezone = TimeZoneInfo.FindSystemTimeZoneById(ConvertTimeZone(timeZone));

            }
            catch (Exception)
            {
                timezone = TimeZoneInfo.FindSystemTimeZoneById(timeZone);

            }

            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            var data = (epoch.AddSeconds((date / 1000))); ;

            var dateTimezone = TimeZoneInfo.ConvertTimeFromUtc(data, timezone);

            return dateTimezone;
        }

        public static long TimestampFromDateTime(DateTime date)
        {
            var timestampData = (date.Ticks - new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero).Ticks) / TimeSpan.TicksPerSecond;
            return (timestampData);
        }

        private class ChaveTimestamp
        {
            public string chave { get; set; }
            public string valor { get; set; }
        }

        public static string ConvertTimeZone(string timeInfo)
        {
            var olsonWindowsTimes = new List<ChaveTimestamp>();

            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Africa/Bangui", valor = "W. Central Africa Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Africa/Cairo", valor = "Egypt Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Africa/Casablanca", valor = "Morocco Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Africa/Harare", valor = "South Africa Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Africa/Johannesburg", valor = "South Africa Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Africa/Lagos", valor = "W. Central Africa Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Africa/Monrovia", valor = "Greenwich Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Africa/Nairobi", valor = "E. Africa Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Africa/Windhoek", valor = "Namibia Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Anchorage", valor = "Alaskan Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Argentina/San_Juan", valor = "Argentina Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Asuncion", valor = "Paraguay Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Bahia", valor = "Bahia Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Bogota", valor = "SA Pacific Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Buenos_Aires", valor = "Argentina Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Caracas", valor = "Venezuela Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Cayenne", valor = "SA Eastern Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Chicago", valor = "Central Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Chihuahua", valor = "Mountain Standard Time (Mexico)" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Cuiaba", valor = "Central Brazilian Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Denver", valor = "Mountain Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Fortaleza", valor = "SA Eastern Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Godthab", valor = "Greenland Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Guatemala", valor = "Central America Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Halifax", valor = "Atlantic Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Indianapolis", valor = "US Eastern Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Indiana/Indianapolis", valor = "US Eastern Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/La_Paz", valor = "SA Western Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Los_Angeles", valor = "Pacific Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Mexico_City", valor = "Mexico Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Montevideo", valor = "Montevideo Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/New_York", valor = "Eastern Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Noronha", valor = "UTC-02" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Phoenix", valor = "US Mountain Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Regina", valor = "Canada Central Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Santa_Isabel", valor = "Pacific Standard Time (Mexico)" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Santiago", valor = "Pacific SA Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Sao_Paulo", valor = "E. South America Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/St_Johns", valor = "Newfoundland Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "America/Tijuana", valor = "Pacific Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Antarctica/McMurdo", valor = "New Zealand Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Atlantic/South_Georgia", valor = "UTC-02" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Almaty", valor = "Central Asia Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Amman", valor = "Jordan Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Baghdad", valor = "Arabic Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Baku", valor = "Azerbaijan Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Bangkok", valor = "SE Asia Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Beirut", valor = "Middle East Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Calcutta", valor = "India Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Colombo", valor = "Sri Lanka Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Damascus", valor = "Syria Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Dhaka", valor = "Bangladesh Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Dubai", valor = "Arabian Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Irkutsk", valor = "North Asia East Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Jerusalem", valor = "Israel Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Kabul", valor = "Afghanistan Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Kamchatka", valor = "Kamchatka Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Karachi", valor = "Pakistan Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Katmandu", valor = "Nepal Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Kolkata", valor = "India Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Krasnoyarsk", valor = "North Asia Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Kuala_Lumpur", valor = "Singapore Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Kuwait", valor = "Arab Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Magadan", valor = "Magadan Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Muscat", valor = "Arabian Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Novosibirsk", valor = "N. Central Asia Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Oral", valor = "West Asia Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Rangoon", valor = "Myanmar Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Riyadh", valor = "Arab Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Seoul", valor = "Korea Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Shanghai", valor = "China Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Singapore", valor = "Singapore Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Taipei", valor = "Taipei Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Tashkent", valor = "West Asia Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Tbilisi", valor = "Georgian Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Tehran", valor = "Iran Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Tokyo", valor = "Tokyo Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Ulaanbaatar", valor = "Ulaanbaatar Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Vladivostok", valor = "Vladivostok Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Yakutsk", valor = "Yakutsk Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Yekaterinburg", valor = "Ekaterinburg Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Asia/Yerevan", valor = "Armenian Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Atlantic/Azores", valor = "Azores Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Atlantic/Cape_Verde", valor = "Cape Verde Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Atlantic/Reykjavik", valor = "Greenwich Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Australia/Adelaide", valor = "Cen. Australia Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Australia/Brisbane", valor = "E. Australia Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Australia/Darwin", valor = "AUS Central Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Australia/Hobart", valor = "Tasmania Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Australia/Perth", valor = "W. Australia Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Australia/Sydney", valor = "AUS Eastern Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Etc/GMT", valor = "UTC" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Etc/GMT+11", valor = "UTC-11" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Etc/GMT+12", valor = "Dateline Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Etc/GMT+2", valor = "UTC-02" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Etc/GMT-12", valor = "UTC+12" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Europe/Amsterdam", valor = "W. Europe Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Europe/Athens", valor = "GTB Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Europe/Belgrade", valor = "Central Europe Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Europe/Berlin", valor = "W. Europe Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Europe/Brussels", valor = "Romance Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Europe/Budapest", valor = "Central Europe Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Europe/Dublin", valor = "GMT Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Europe/Helsinki", valor = "FLE Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Europe/Istanbul", valor = "GTB Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Europe/Kiev", valor = "FLE Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Europe/London", valor = "GMT Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Europe/Minsk", valor = "E. Europe Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Europe/Moscow", valor = "Russian Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Europe/Paris", valor = "Romance Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Europe/Sarajevo", valor = "Central European Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Europe/Warsaw", valor = "Central European Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Indian/Mauritius", valor = "Mauritius Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Pacific/Apia", valor = "Samoa Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Pacific/Auckland", valor = "New Zealand Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Pacific/Fiji", valor = "Fiji Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Pacific/Guadalcanal", valor = "Central Pacific Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Pacific/Guam", valor = "West Pacific Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Pacific/Honolulu", valor = "Hawaiian Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Pacific/Pago_Pago", valor = "UTC-11" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Pacific/Port_Moresby", valor = "West Pacific Standard Time" });
            olsonWindowsTimes.Add(new ChaveTimestamp { chave = "Pacific/Tongatapu", valor = "Tonga Standard Time" });

            string timeInfoKey = string.Empty;
            if (olsonWindowsTimes.Find(p => p.chave == timeInfo) != null)
                return olsonWindowsTimes.Find(p => p.chave == timeInfo).valor;
            else
                return "UTC";
        }
    }

    public static class Extensions
    {
        public static bool IsPrimitive(this PropertyInfo p)
        {
            Type t = p.PropertyType;
            // TODO: put any type here that you consider as primitive as I didn't
            // quite understand what your definition of primitive type is
            return new[] {
                typeof(string),
                typeof(String),
                typeof(char),
                typeof(byte),
                typeof(Byte),
                typeof(SByte),
                typeof(sbyte),
                typeof(ushort),
                typeof(short),
                typeof(Int16),
                typeof(UInt16),
                typeof(uint),
                typeof(int),
                typeof(Int32),
                typeof(UInt32),
                typeof(Int64),
                typeof(UInt64),
                typeof(ulong),
                typeof(long),
                typeof(float),
                typeof(double),
                typeof(Double),
                typeof(decimal),
                typeof(Decimal),
                typeof(DateTime),
                typeof(bool),
                typeof(Boolean),
                typeof(byte?),
                typeof(Byte?),
                typeof(SByte?),
                typeof(sbyte?),
                typeof(ushort?),
                typeof(short?),
                typeof(Int16?),
                typeof(UInt16?),
                typeof(uint?),
                typeof(int?),
                typeof(Int32?),
                typeof(UInt32?),
                typeof(Int64?),
                typeof(UInt64?),
                typeof(ulong?),
                typeof(long?),
                typeof(float?),
                typeof(double?),
                typeof(Double?),
                typeof(decimal?),
                typeof(Decimal?),
                typeof(DateTime?),
                typeof(bool?),
                typeof(Boolean?),
            }.Contains(t);
        }

        public static Expression<TDelegate> AndAlso<TDelegate>(this Expression<TDelegate> left, Expression<TDelegate> right)
        {
            return Expression.Lambda<TDelegate>(Expression.AndAlso(left.Body, new ExpressionParameterReplacer(right.Parameters, left.Parameters).Visit(right.Body)), left.Parameters);
        }
    }

    public class ExpressionParameterReplacer : EntityGenericFlow.Infrastructure.ExpressionVisitor.ExpressionVisitor
    {
        public ExpressionParameterReplacer(IList<ParameterExpression> fromParameters, IList<ParameterExpression> toParameters)
        {
            ParameterReplacements = new Dictionary<ParameterExpression, ParameterExpression>();
            for (int i = 0; i != fromParameters.Count && i != toParameters.Count; i++)
                ParameterReplacements.Add(fromParameters[i], toParameters[i]);
        }
        private static IDictionary<ParameterExpression, ParameterExpression> ParameterReplacements
        {
            get;
            set;
        }
        protected override Expression VisitParameter(ParameterExpression node)
        {
            ParameterExpression replacement;
            if (ParameterReplacements.TryGetValue(node, out replacement))
                node = replacement;
            return base.VisitParameter(node);
        }
    }

    public class IsUnique : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            bool result = true;
            return result;
        }
    }
}
