﻿using EntityGenericFlow.Infrastructure;
using EntityGenericFlow.Infrastructure.Library;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace EntityGenericFlow.Core
{
    public class Generic<T, U> : IGeneric<T> where T : class
    {
        public virtual Lib.Entity<T> Save(T Object, bool lazyLoad = false)
        {
            var resultado = new Lib.Entity<T>();

            var obj = (object)Object;

            try
            {
                object finded = null;
                EntityEntry entry = null;
                DbSet<T> dbSet = null;
                obj = (object)Object;
                PropertyInfo[] chavesPrimarias = obj.GetType().GetProperties().Where(p => p.CustomAttributes.Any(attr => attr.AttributeType == typeof(KeyAttribute))).ToArray();
                List<object> keyValues;

                if (chavesPrimarias == null || chavesPrimarias.Length == 0)
                    chavesPrimarias = obj.GetType().GetProperties().Where(p => p.PropertyType == typeof(int) || p.PropertyType == typeof(decimal) || p.PropertyType == typeof(Int64) || p.PropertyType == typeof(double)).ToArray();

                if (chavesPrimarias == null || chavesPrimarias.Length == 0)
                    chavesPrimarias = obj.GetType().GetProperties();

                using (var db = (DbContext)Activator.CreateInstance(typeof(U)))
                {
                    entry = db.Entry(obj);

                    dbSet = db.Set<T>();
                    keyValues = new List<object>();

                    foreach (var chave in chavesPrimarias)
                        keyValues.Add(entry.Property(chave.Name).CurrentValue);

                    finded = dbSet.Find(keyValues.ToArray());
                }

                using (var db = (DbContext)Activator.CreateInstance(typeof(U)))
                {
                    bool isUpdate = finded != null;

                    if (VerifyUniqueExists(obj, isUpdate, chavesPrimarias, keyValues))
                    {
                        resultado.Success = false;
                        resultado.Message = "Não foi possível salvar o registro. Já existe este registro na base de dados.";

                        return resultado;
                    }


                    var props = obj.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);
                    var propsPrimitive = props.Where(p => p.IsPrimitive()).ToArray();
                    if (propsPrimitive != null && propsPrimitive.Length > 0
                           && propsPrimitive.Any(p => p.Name.Trim().ToUpper().Equals("ATIVO")))
                    {
                        var propAtivo = propsPrimitive.Where(x => x.Name.Trim().ToUpper().Equals("ATIVO")).FirstOrDefault();

                        if (!isUpdate || propAtivo.GetValue(obj, null) == null)
                            propAtivo.SetValue(obj, true, null);
                    }

                    entry = db.Entry(obj);
                    entry.State = isUpdate ? EntityState.Modified : EntityState.Added;

                    int retlinhas = db.SaveChanges();

                    resultado.Object = ((T)obj);

                    if (retlinhas == 0)
                    {
                        resultado.Success = false;
                        resultado.Message = "O registro não foi salvo.";
                    }
                }
            }
            catch (Exception ex)
            {
                resultado.Success = false;
                resultado.Message = ex.Message;

                throw ex;
            }

            return resultado;
        }

        private static bool VerifyUniqueExists(object obj, bool isUpdate = false, PropertyInfo[] primaryKeys = null, List<object> valuesKeys = null)
        {
            using (var db = (DbContext)Activator.CreateInstance(typeof(U)))
            {
                PropertyInfo[] uniques = obj.GetType().GetProperties().Where(p => p.CustomAttributes.Any(attr => attr.AttributeType == typeof(IsUnique))).ToArray();

                if (uniques == null || uniques.Length == 0)
                    return false;

                Expression<Func<T, bool>> lambda = l => true;

                if (isUpdate)
                {
                    for (int i = 0; i < primaryKeys.Length; i++)
                        lambda = CreateLambdaItem(lambda, primaryKeys[i].Name, valuesKeys[i], false);
                }

                IQueryable<T> query = db.Set<T>();

                foreach (var propUn in uniques)
                    lambda = CreateLambdaItem(lambda, propUn.Name, propUn.GetValue(obj, null));

                return query.Where(lambda).Count() > 0;
            }
        }

        private static Expression<Func<T, bool>> CreateLambdaItem(Expression<Func<T, bool>> lambda, string propName, object propVal, bool isEqual = true)
        {
            var item = Expression.Parameter(typeof(T), "f");

            var propNameSplit = propName.Split(".");

            MemberExpression prop = Expression.Property(item, propNameSplit.FirstOrDefault());

            if (propNameSplit.Length > 1)
                prop = Expression.Property(prop, propNameSplit.LastOrDefault());

            var soap = Expression.Convert(Expression.Constant(propVal), prop.Type);
            var equal = isEqual ? Expression.Equal(prop, soap) : Expression.NotEqual(prop, soap);

            return lambda.AndAlso(Expression.Lambda<Func<T, bool>>(equal, item));
        }

        public virtual Lib.Collection<T> SaveAll(List<T> ObjectList, string nameKey = null)
        {
            var resultado = new Lib.Collection<T>();

            object obj;
            EntityEntry entry;
            int resultados = 0;
            int retlinhas = 0;
            DbSet<T> dbSet;
            object finded;
            PropertyInfo[] chavesPrimarias;
            List<object> keyValues;

            try
            {
                foreach (var Object in ObjectList)
                {
                    finded = null;
                    entry = null;
                    dbSet = null;
                    obj = (object)Object;

                    if (string.IsNullOrEmpty(nameKey))
                    {
                        chavesPrimarias = obj.GetType().GetProperties().Where(p => p.CustomAttributes.Any(attr => attr.AttributeType == typeof(KeyAttribute))).ToArray();

                        var props = obj.GetType().GetProperties();
                        if (chavesPrimarias == null || chavesPrimarias.Length == 0)
                            chavesPrimarias = obj.GetType().GetProperties().Where(p => p.PropertyType == typeof(int) || p.PropertyType == typeof(decimal) || p.PropertyType == typeof(Int64) || p.PropertyType == typeof(double)).ToArray();

                        if (chavesPrimarias == null || chavesPrimarias.Length == 0)
                            chavesPrimarias = obj.GetType().GetProperties();
                    }
                    else
                        chavesPrimarias = obj.GetType().GetProperties().Where(p => p.Name.Trim().ToUpper().Equals(nameKey.Trim().ToUpper())).ToArray();

                    using (var db = (DbContext)Activator.CreateInstance(typeof(U)))
                    {
                        entry = db.Entry(obj);

                        dbSet = db.Set<T>();
                        keyValues = new List<object>();

                        foreach (var chave in chavesPrimarias)
                            keyValues.Add(entry.Property(chave.Name).CurrentValue);

                        finded = dbSet.Find(keyValues.ToArray());
                    }

                    using (var db = (DbContext)Activator.CreateInstance(typeof(U)))
                    {
                        var props = obj.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);
                        var propsPrimitive = props.Where(p => p.IsPrimitive()).ToArray();
                        if (propsPrimitive != null && propsPrimitive.Length > 0
                               && propsPrimitive.Any(p => p.Name.Trim().ToUpper().Equals("ATIVO")))
                        {
                            var propAtivo = propsPrimitive.Where(x => x.Name.Trim().ToUpper().Equals("ATIVO")).FirstOrDefault();

                            if (finded == null || propAtivo.GetValue(obj, null) == null)
                                propAtivo.SetValue(obj, true, null);
                        }

                        if (VerifyUniqueExists(obj,  finded != null, chavesPrimarias, keyValues))
                        {
                            resultado.Success = false;
                            resultado.Message = "Não foi possível salvar o registro. Já existe este registro na base de dados.";

                            return resultado;
                        }

                        entry = db.Entry(obj);

                        if (finded != null)
                            entry.State = EntityState.Modified;
                        else
                            entry.State = EntityState.Added;

                        retlinhas = db.SaveChanges();

                        if (retlinhas == 1)
                            resultados++;
                    }
                }

                resultado.List = ObjectList;
                resultado.Total = ObjectList.Count;

                if (resultados == ObjectList.Count)
                {
                    resultado.Success = true;
                    resultado.Message = "Todos os registros foram salvos com sucesso.";
                }
                else
                {
                    resultado.Success = false;
                    resultado.Message = string.Format("Alguns registros não puderem ser salvos: {0} itens não salvos.", (ObjectList.Count - resultados).ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.Success = false;
                resultado.Message = ex.Message;

                throw ex;
            }

            return resultado;
        }

        public Lib.Entity<T> Delete(T Object, bool cascade = true, bool logic = true)
        {
            var resultado = new Lib.Entity<T>();
            var obj = (object)Object;

            try
            {
                using (var db = (DbContext)Activator.CreateInstance(typeof(U)))
                {
                    //Set null in objects that is not list or array and not is primitive.
                    var props = obj.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);
                    if (props != null && props.Length > 0)
                    {
                        if (cascade)
                        {
                            var propsClass = props.Where(p => !p.IsPrimitive()).ToArray();

                            var propsList = propsClass.Where(p => p.PropertyType.IsGenericType);
                            if (propsList != null && propsList.Count() > 0)
                            {
                                foreach (var prop in propsClass.Where(x => !propsList.Any(p => p.Name.Equals(x.Name))))
                                    prop.SetValue(obj, null, null);
                            }
                        }

                        var propsPrimitive = props.Where(p => p.IsPrimitive()).ToArray();

                        EntityEntry entry;
                        if (logic && propsPrimitive != null && propsPrimitive.Length > 0
                            && propsPrimitive.Any(p => p.Name.Trim().ToUpper().Equals("ATIVO")))
                        {
                            var propAtivo = propsPrimitive.Where(x => x.Name.Trim().ToUpper().Equals("ATIVO")).FirstOrDefault();
                            propAtivo.SetValue(obj, false, null);

                            entry = db.Update(obj);
                        }
                        else
                            entry = db.Remove(obj);

                        int retlinhas = db.SaveChanges();
                        resultado.Object = ((T)obj);

                        if (retlinhas == 0)
                        {
                            resultado.Success = false;
                            resultado.Message = "O registro não foi excluído";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                resultado.Success = false;
                resultado.Message = ex.Message;

                throw ex;
            }

            return resultado;
        }

        public Lib.Collection<T> ListAll(bool lazyLoading = false, bool excluded = false, bool allObjNotExcluded = false)
        {
            return ListAll<Object>(lazyLoading: lazyLoading, excluded: excluded, allObjNotExcluded: allObjNotExcluded);
        }

        public Lib.Collection<T> ListAll<K>(Expression<Func<T, K>> orderBy = null, bool asc = true, bool lazyLoading = false, bool excluded = false, bool allObjNotExcluded = false)
        {
            var resultado = new Lib.Collection<T>();

            try
            {
                using (var db = (DbContext)Activator.CreateInstance(typeof(U)))
                {
                    IQueryable<T> query = db.Set<T>();

                    if (orderBy != null)
                    {
                        if (asc)
                            query = query.OrderBy(orderBy);
                        else
                            query = query.OrderByDescending(orderBy);
                    }

                    var objList = Activator.CreateInstance<T>();
                    var props = objList.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);

                    Expression<Func<T, bool>> lambda = l => true;

                    lambda = CreateLambdaActive(lazyLoading, excluded, ref query, lambda, allObjNotExcluded);

                    query = query.Where(lambda);

                    resultado.List = query.ToList();
                    resultado.Total = resultado.List.Count;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resultado;
        }

        public Lib.Collection<T> Filter(Expression<Func<T, bool>> filter = null, bool lazyLoading = false, bool excluded = false, bool allObjNotExcluded = false)
        {
            return Filter<Object>(filter, lazyLoading: lazyLoading, excluded: excluded, allObjNotExcluded: allObjNotExcluded);
        }

        public Lib.Collection<T> Filter<K>(Expression<Func<T, bool>> filter = null, Expression<Func<T, K>> orderBy = null, bool asc = true, bool lazyLoading = false, bool excluded = false, bool allObjNotExcluded = false)
        {
            var resultado = new Lib.Collection<T>();

            try
            {
                using (var db = (DbContext)Activator.CreateInstance(typeof(U)))
                {
                    IQueryable<T> query = db.Set<T>();

                    if (orderBy != null)
                    {
                        if (asc)
                            query = query.OrderBy(orderBy);
                        else
                            query = query.OrderByDescending(orderBy);
                    }

                    if (filter == null)
                        filter = f => true;

                    filter = CreateLambdaActive(lazyLoading, excluded, ref query, filter, allObjNotExcluded);

                    query = query.Where(filter);

                    resultado.List = query.ToList();
                }
            }
            catch (Exception ex)
            {
                resultado.Success = false;
                resultado.Message = ex.Message;

                throw ex;
            }

            return resultado;
        }

        public decimal? GetSum(Expression<Func<T, decimal?>> filter = null)
        {
            try
            {
                using (var db = (DbContext)Activator.CreateInstance(typeof(U)))
                {
                    IQueryable<T> query = db.Set<T>();

                    return query.Sum(filter);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal? GetAverage(Expression<Func<T, decimal?>> filter = null)
        {
            try
            {
                using (var db = (DbContext)Activator.CreateInstance(typeof(U)))
                {
                    IQueryable<T> query = db.Set<T>();

                    return query.Average(filter);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public long GetTotal(Expression<Func<T, bool>> filter = null, bool excluded = false, bool allObjNotExcluded = false)
        {
            try
            {
                using (var db = (DbContext)Activator.CreateInstance(typeof(U)))
                {
                    IQueryable<T> query = db.Set<T>();

                    if (filter == null)
                        filter = f => true;

                    filter = CreateLambdaActive(false, excluded, ref query, filter, allObjNotExcluded);

                    return query.LongCount(filter);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Lib.Entity<T> GetFirst(Expression<Func<T, bool>> filter = null, bool lazyLoading = false, bool excluded = false, bool allObjNotExcluded = false)
        {
            return GetFirst<Object>(filter, lazyLoading: lazyLoading, excluded: excluded, allObjNotExcluded: allObjNotExcluded);
        }

        public Lib.Entity<T> GetFirst<K>(Expression<Func<T, bool>> filter = null, Expression<Func<T, K>> orderBy = null, bool asc = true, bool lazyLoading = false, bool excluded = false, bool allObjNotExcluded = false)
        {
            Lib.Entity<T> resultado = new Lib.Entity<T>();

            try
            {
                using (var db = (DbContext)Activator.CreateInstance(typeof(U)))
                {
                    IQueryable<T> query = db.Set<T>();

                    if (orderBy != null)
                    {
                        if (asc)
                            query = query.OrderBy(orderBy);
                        else
                            query = query.OrderByDescending(orderBy);
                    }

                    if (filter == null)
                        filter = f => true;

                    filter = CreateLambdaActive(lazyLoading, excluded, ref query, filter, allObjNotExcluded);

                    query = query.Where(filter);

                    resultado.Object = query.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                resultado.Success = false;
                resultado.Message = ex.Message;

                throw ex;
            }

            return resultado;
        }

        private static Expression<Func<T, bool>> CreateLambdaActive(bool lazyLoading, bool excluded, ref IQueryable<T> query, Expression<Func<T, bool>> lambda, bool allObjNotExcluded = false)
        {
            var objList = Activator.CreateInstance<T>();
            var props = objList.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);

            if (lazyLoading || !excluded)
            {
                foreach (var includeProperty in props.Where(p => !p.IsPrimitive()))
                {
                    if (lazyLoading)
                        query = query.Include(includeProperty.Name);

                    if (!excluded && allObjNotExcluded)
                    {
                        var propsInsideClass = includeProperty.PropertyType.GetProperties(BindingFlags.Instance | BindingFlags.Public);
                        var propsPrimitiveClass = propsInsideClass.Where(p => p.IsPrimitive()).ToArray();

                        if (!includeProperty.PropertyType.IsGenericType && propsPrimitiveClass != null
                            && propsPrimitiveClass.Length > 0 && propsPrimitiveClass.Any(p => p.Name.Trim().ToUpper().Equals("ATIVO")))
                        {
                            var propNameInside = string.Format("{0}.{1}", includeProperty.Name, "Ativo");
                            lambda = CreateLambdaItem(lambda, propNameInside, true);
                        }
                    }
                }
            }

            var propsPrimitive = props.Where(p => p.IsPrimitive()).ToArray();
            if (!excluded && propsPrimitive != null && propsPrimitive.Length > 0
                   && propsPrimitive.Any(p => p.Name.Trim().ToUpper().Equals("ATIVO")))
            {
                lambda = CreateLambdaItem(lambda, "Ativo", true);
            }

            return lambda;
        }
    }
}