﻿using EntityGenericFlow.Infrastructure.Library;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EntityGenericFlow.Core
{
    public interface IGeneric<T> where T : class
    {
        Lib.Entity<T> Save(T Object, bool lazyLoad = false);
        Lib.Collection<T> SaveAll(List<T> ObjectList, string nameKey = null);
        Lib.Entity<T> Delete(T Object, bool cascade = true, bool logic = true);
        Lib.Collection<T> ListAll(bool lazyLoading = false, bool excluded = false, bool allObjNotExcluded = false);
        Lib.Collection<T> ListAll<K>(Expression<Func<T, K>> orderBy = null, bool asc = true, bool lazyLoading = false, bool excluded = false, bool allObjNotExcluded = false);
        Lib.Collection<T> Filter(Expression<Func<T, bool>> filter = null, bool lazyLoading = false, bool excluded = false, bool allObjNotExcluded = false);
        Lib.Collection<T> Filter<K>(Expression<Func<T, bool>> filter = null, Expression<Func<T, K>> orderBy = null, bool asc = true, bool lazyLoading = false, bool excluded = false, bool allObjNotExcluded = false);
        Lib.Entity<T> GetFirst(Expression<Func<T, bool>> filter = null, bool lazyLoading = false, bool excluded = false, bool allObjNotExcluded = false);
        Lib.Entity<T> GetFirst<K>(Expression<Func<T, bool>> filter = null, Expression<Func<T, K>> orderBy = null, bool asc = true, bool lazyLoading = false, bool excluded = false, bool allObjNotExcluded = false);
        long GetTotal(Expression<Func<T, bool>> filter = null, bool excluded = false, bool allObjNotExcluded = false);
        decimal? GetSum(Expression<Func<T, decimal?>> filter = null);
        decimal? GetAverage(Expression<Func<T, decimal?>> filter = null);
    }
}
